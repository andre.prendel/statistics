```
Erz. VW - erziehlter Verkaufswert (Umsatz)
VJ (Vorjahr)
VJ% (Abweichung in %)
TQ (Trefferquote im Zeitraum, Besucher total / Besucher mit Bon)
Teile pro Bon (T/B)
Durchn. Preis (P/T) oder d.Bon
SL - Stundenleistung (Umsatz pro (Arbeits-)Stunde eines MA)
NL - Nachlass
---
PE - Planerfüllung
---
```

# Planerfüllung (PE)

```
\\
PE \quad \text{Planerf\"ullung} \\
U_{soll} \quad \text{geplanter Umsatz} \\
U_{ist} \quad \text{tats\"achlicher Umsatz} \\
\begin{align*}
\textbf{Formel:} \\
PE &= \frac{U_{ist}}{U_{soll}} \\
\textbf{Beispiel:} \\
\frac{64561}{56000} &= 1,152875 \approx 115,3 \%
\end{align*}
```

![PE](./planerfuellung.png)

# Umsatzwachstum (VJ/Ist)

```
\\
U_w \quad \text{Umsatzwachstum} \\
U_{aj} \quad \text{Umsatz aktuelles Jahr} \\
U_{vj} \quad \text{Umsatz vorheriges Jahr} \\
\begin{align*}
\textbf{Formel:} \\
U_w &= \frac{U_{aj}}{U_{vj}} \\
\textbf{Beispiel:} \\
\frac{64561}{54140}-1 &= 0,192482453 \approx 19,2 \%
\end{align*}
```

![Umsatzwachstum](./umsatzwachstum.png)

# Durchschittlicher Bonwert

```
\\
Bon_\phi \quad \text{Durchschnittlicher Bonwert} \\
T_{Bon} \quad \text{Teile pro Bon} \\
T_\phi \quad \text{Durchschittlicher Preis pro Teil} \\
\begin{align*}
\textbf{Formel:} \\
Bon_\phi = T_{Bon} \times T_\phi \\
\textbf{Beispiel:} \\
1.8261986301369864 \times 30.267829348335674 \approx 55,28
\end{align*}


\newline
Bon_\phi := \text{Durchschnittlicher Bonwert}
\newline
T_{Bon} := \text{Teile pro Bon}
\newline
T_\phi := \text{Durchschittlicher Preis pro Teil}
\newline
\newline
Bon_\phi = T_{Bon} \times T_\phi
\newline
\newline
\textbf{Beispiel:}
\newline
\newline
1.8261986301369864 \times 30.267829348335674 \approx 55,28
```

![Bon](./durchschnitt-bon.png)

# Trefferquote

```
\newline
TQ := \text{Trefferquote}
\newline
Bon_{\sum} := \text{Anzahl Bons}
\newline
Bes_{\sum} := \text{Anzahl Besucher}
\newline
\newline
TQ = \frac{Bon_{\sum}}{Bes_{\sum}}
\textbf{Beispiel:}
\newline
\newline
\frac{1168}{8963} = 0,130313511 \approx 13 \%
```

![Quote](./trefferquote.png)


```
http://pt-dan/api_dev/store/d8a6356e-19e4-44df-be6f-270e57ebc0c6/stats/fact/monthly/from/year/2019/month/6/to/year/2019/month/6

[
    {
        "storeId": "d8a6356e-19e4-44df-be6f-270e57ebc0c6",
        "storeName": "Esprit Buchholz",
        "monthZonedByLocation": 201906,
        "countVisitors": 8963,
        "revenuePositiveSum": 66820.700000000012,
        "revenueNegativeSum": 2259.42,
        "revenueDifferenceSum": 64561.279999999992,
        "countReceiptsWithPositiveActingItems": 1192,
        "countReceiptsWithPositiveRevenue": 1168,
        "countReceiptsWithNeutralRevenue": 10,
        "countReceiptsWithNegativeRevenue": 44,
        "countItemsSold": 2205,
        "countItemsReturned": 72,
        "countItemsSoldMinusReturned": 2133,
        "ratioItemsReturnedToAllItems": 0.031620553359683792,
        "timeBookingDurationFactTicks": 14831400000000,
        "hitRatio": 0.1303135111011938,
        "itemPerReceipt": 1.8261986301369864,
        "meanRevenuePerItem": 30.267829348335674,
        "timeBookingDurationFactTotalHours": 411.98333333333329,
        "hourlyOutput": 156.70847526194424,
        "timeBookingDurationFactTotalMinutes": 24719,
        "timeFactor": 2.7578935624232956
    }
]
```
