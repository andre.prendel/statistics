# http://isp.pth.group/isp2/stats/

```
GET /api2/brand HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
Cache-Control: max-age=0
```

```
[
    {
        "id": "364bb1c1-1df2-4172-a908-25cc6800d34f",
        "name": "Esprit"
    },
    {
        "id": "560f8233-d782-4041-bba2-2ae69e75d2e6",
        "name": "G-Star"
    },
    {
        "id": "76a5de0d-b70d-42b0-9f77-51f8c5c7420e",
        "name": "Tom Tailor"
    },
    {
        "id": "6cbcd489-d83c-414e-9d0f-8283a2419748",
        "name": "Levi´s"
    },
    {
        "id": "5ac23c60-6d8c-4521-9aaf-1b405e0ccd8a",
        "name": "Marc O´Polo"
    },
    {
        "id": "f88161f2-c726-4c89-a72d-80c2731f38f2",
        "name": "More & More"
    },
    {
        "id": "918e2529-4ab2-4753-9d0e-0613566bb8c4",
        "name": "Calvin Klein"
    },
    {
        "id": "d262bf5a-c248-44dc-8f4a-d7af098b8089",
        "name": "Tommy Hilfiger"
    },
    {
        "id": "aa552227-c21a-4ab6-b227-7d5ad090e7f3",
        "name": "Esprit CZ"
    },
    {
        "id": "80385468-ed57-4924-a84a-bac44469b6b8",
        "name": "More & More CZ"
    }
]
```

```
GET /api2/brand/364bb1c1-1df2-4172-a908-25cc6800d34f/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "364bb1c1-1df2-4172-a908-25cc6800d34f",
    "brandName": "Esprit",
    "yearZonedByLocation": 2019,
    "countVisitors": 7206652,
    "countReceiptsWithPositiveRevenue": 835288.50706,
    "countItemsSoldMinusReturned": 1470955.5864869999,
    "revenueDifference": 45704953.070668,
    "hitRatio": 0.11590520911235896,
    "itemPerReceipt": 1.7610149954826795,
    "meanRevenuePerItem": 31.07160643770527
}
```

```
GET /api2/brand/364bb1c1-1df2-4172-a908-25cc6800d34f/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "364bb1c1-1df2-4172-a908-25cc6800d34f",
    "brandName": "Esprit",
    "yearZonedByLocation": 2019,
    "countVisitors": 6582899,
    "revenuePositiveSum": 40844082.27,
    "revenueNegativeSum": 1345394.2100000002,
    "revenueDifferenceSum": 42106952.63,
    "countReceiptsWithPositiveActingItems": 706732,
    "countReceiptsWithPositiveRevenue": 741167,
    "countReceiptsWithNeutralRevenue": 5103,
    "countReceiptsWithNegativeRevenue": 19469,
    "countItemsSold": 1279836,
    "countItemsReturned": 34800,
    "countItemsSoldMinusReturned": 1323347,
    "ratioItemsReturnedToAllItems": 0.026471205717780437,
    "timeBookingDurationFactTicks": 13165666800000000,
    "hitRatio": 0.11258975718752483,
    "itemPerReceipt": 1.7854909892102591,
    "meanRevenuePerItem": 31.81852728724968,
    "timeBookingDurationFactTotalHours": 365712.96666666667,
    "hourlyOutput": 115.136613869037,
    "timeBookingDurationFactTotalMinutes": 21942778,
    "timeFactor": 3.3333001159519537
}
```

```
GET /api2/brand/560f8233-d782-4041-bba2-2ae69e75d2e6/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "560f8233-d782-4041-bba2-2ae69e75d2e6",
    "brandName": "G-Star",
    "yearZonedByLocation": 2019,
    "countVisitors": 589879,
    "countReceiptsWithPositiveRevenue": 64170.11959,
    "countItemsSoldMinusReturned": 103020.14068,
    "revenueDifference": 8369161.808849,
    "hitRatio": 0.10878522474948253,
    "itemPerReceipt": 1.605422295271119,
    "meanRevenuePerItem": 81.2381127962657
}
```

```
GET /api2/brand/560f8233-d782-4041-bba2-2ae69e75d2e6/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "560f8233-d782-4041-bba2-2ae69e75d2e6",
    "brandName": "G-Star",
    "yearZonedByLocation": 2019,
    "countVisitors": 529221,
    "revenuePositiveSum": 8290784.330000001,
    "revenueNegativeSum": 301165.68,
    "revenueDifferenceSum": 7989618.6499999994,
    "countReceiptsWithPositiveActingItems": 62574,
    "countReceiptsWithPositiveRevenue": 61469,
    "countReceiptsWithNeutralRevenue": 899,
    "countReceiptsWithNegativeRevenue": 1445,
    "countItemsSold": 103466,
    "countItemsReturned": 3354,
    "countItemsSoldMinusReturned": 100112,
    "ratioItemsReturnedToAllItems": 0.031398614491668225,
    "timeBookingDurationFactTicks": 2527505700000000,
    "hitRatio": 0.11614996381473902,
    "itemPerReceipt": 1.6286583481104298,
    "meanRevenuePerItem": 79.806802880773532,
    "timeBookingDurationFactTotalHours": 70208.491666666669,
    "hourlyOutput": 113.79846597378592,
    "timeBookingDurationFactTotalMinutes": 4212509.5,
    "timeFactor": 7.9598305811749723
}
```

```
GET /api2/brand/76a5de0d-b70d-42b0-9f77-51f8c5c7420e/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
null
```

```
GET /api2/brand/76a5de0d-b70d-42b0-9f77-51f8c5c7420e/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "76a5de0d-b70d-42b0-9f77-51f8c5c7420e",
    "brandName": "Tom Tailor",
    "yearZonedByLocation": 2019,
    "countVisitors": 1842047,
    "revenuePositiveSum": 12189108.11,
    "revenueNegativeSum": 339480.42000000004,
    "revenueDifferenceSum": 11849627.690000003,
    "countReceiptsWithPositiveActingItems": 207782,
    "countReceiptsWithPositiveRevenue": 205245,
    "countReceiptsWithNeutralRevenue": 1904,
    "countReceiptsWithNegativeRevenue": 4360,
    "countItemsSold": 367011,
    "countItemsReturned": 9038,
    "countItemsSoldMinusReturned": 357973,
    "ratioItemsReturnedToAllItems": 0.024034101938843075,
    "timeBookingDurationFactTicks": 4534444800000000,
    "hitRatio": 0.11142223841194063,
    "itemPerReceipt": 1.7441253136495407,
    "meanRevenuePerItem": 33.102015207850883,
    "timeBookingDurationFactTotalHours": 125956.79999999999,
    "hourlyOutput": 94.076919150057833,
    "timeBookingDurationFactTotalMinutes": 7557408,
    "timeFactor": 4.1027226775429728
}
```

```
GET /api2/brand/6cbcd489-d83c-414e-9d0f-8283a2419748/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "6cbcd489-d83c-414e-9d0f-8283a2419748",
    "brandName": "Levi´s",
    "yearZonedByLocation": 2019,
    "countVisitors": 763557,
    "countReceiptsWithPositiveRevenue": 89021.553810000012,
    "countItemsSoldMinusReturned": 126846.17333899999,
    "revenueDifference": 6565839.7698890008,
    "hitRatio": 0.11658796109524242,
    "itemPerReceipt": 1.4248928255030193,
    "meanRevenuePerItem": 51.762221886990694
}
```

```
GET /api2/brand/6cbcd489-d83c-414e-9d0f-8283a2419748/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "6cbcd489-d83c-414e-9d0f-8283a2419748",
    "brandName": "Levi´s",
    "yearZonedByLocation": 2019,
    "countVisitors": 693531,
    "revenuePositiveSum": 6441311.14,
    "revenueNegativeSum": 82674.82,
    "revenueDifferenceSum": 6360671.5799999991,
    "countReceiptsWithPositiveActingItems": null,
    "countReceiptsWithPositiveRevenue": 78953,
    "countReceiptsWithNeutralRevenue": 1238,
    "countReceiptsWithNegativeRevenue": 1242,
    "countItemsSold": 116203,
    "countItemsReturned": 1097,
    "countItemsSoldMinusReturned": 115106,
    "ratioItemsReturnedToAllItems": 0.0093520886615515754,
    "timeBookingDurationFactTicks": 2036829600000000,
    "hitRatio": 0.11384206329637753,
    "itemPerReceipt": 1.4579053360860259,
    "meanRevenuePerItem": 55.259253036331721,
    "timeBookingDurationFactTotalHours": 56578.6,
    "hourlyOutput": 112.42186232957336,
    "timeBookingDurationFactTotalMinutes": 3394716,
    "timeFactor": 4.89482950293498
}
```

```
GET /api2/brand/5ac23c60-6d8c-4521-9aaf-1b405e0ccd8a/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "5ac23c60-6d8c-4521-9aaf-1b405e0ccd8a",
    "brandName": "Marc O´Polo",
    "yearZonedByLocation": 2019,
    "countVisitors": 291642,
    "countReceiptsWithPositiveRevenue": 21767.08341,
    "countItemsSoldMinusReturned": 37330.398683,
    "revenueDifference": 2300242.469921,
    "hitRatio": 0.07463631236241694,
    "itemPerReceipt": 1.7149931380264785,
    "meanRevenuePerItem": 61.6184812129669
}
```

```
GET /api2/brand/5ac23c60-6d8c-4521-9aaf-1b405e0ccd8a/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "5ac23c60-6d8c-4521-9aaf-1b405e0ccd8a",
    "brandName": "Marc O´Polo",
    "yearZonedByLocation": 2019,
    "countVisitors": 276632,
    "revenuePositiveSum": 2252506.14,
    "revenueNegativeSum": 84344.159999999989,
    "revenueDifferenceSum": 2174736.05,
    "countReceiptsWithPositiveActingItems": 19710,
    "countReceiptsWithPositiveRevenue": 19506,
    "countReceiptsWithNeutralRevenue": 151,
    "countReceiptsWithNegativeRevenue": 621,
    "countItemsSold": 32449,
    "countItemsReturned": 1043,
    "countItemsSoldMinusReturned": 31406,
    "ratioItemsReturnedToAllItems": 0.031141765197659144,
    "timeBookingDurationFactTicks": 830732400000000,
    "hitRatio": 0.0705124497527401,
    "itemPerReceipt": 1.6100686968112377,
    "meanRevenuePerItem": 69.245878176144686,
    "timeBookingDurationFactTotalHours": 23075.899999999998,
    "hourlyOutput": 94.242740261484926,
    "timeBookingDurationFactTotalMinutes": 1384554,
    "timeFactor": 5.0050391856328984
}
```

```
GET /api2/brand/f88161f2-c726-4c89-a72d-80c2731f38f2/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "f88161f2-c726-4c89-a72d-80c2731f38f2",
    "brandName": "More & More",
    "yearZonedByLocation": 2019,
    "countVisitors": 458394,
    "countReceiptsWithPositiveRevenue": 43408.60474,
    "countItemsSoldMinusReturned": 75503.093551,
    "revenueDifference": 3705341.898642,
    "hitRatio": 0.094697148610147608,
    "itemPerReceipt": 1.7393577610529758,
    "meanRevenuePerItem": 49.075365317835043
}
```

```
GET /api2/brand/f88161f2-c726-4c89-a72d-80c2731f38f2/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "f88161f2-c726-4c89-a72d-80c2731f38f2",
    "brandName": "More & More",
    "yearZonedByLocation": 2019,
    "countVisitors": 414873,
    "revenuePositiveSum": 3599935.2299999995,
    "revenueNegativeSum": 111470.73999999998,
    "revenueDifferenceSum": 3488464.4899999993,
    "countReceiptsWithPositiveActingItems": 38677,
    "countReceiptsWithPositiveRevenue": 38236,
    "countReceiptsWithNeutralRevenue": 312,
    "countReceiptsWithNegativeRevenue": 981,
    "countItemsSold": 67259,
    "countItemsReturned": 1780,
    "countItemsSoldMinusReturned": 65479,
    "ratioItemsReturnedToAllItems": 0.025782528715653473,
    "timeBookingDurationFactTicks": 1395517800000000,
    "hitRatio": 0.092163143901868763,
    "itemPerReceipt": 1.7124960769955015,
    "meanRevenuePerItem": 53.276080728172381,
    "timeBookingDurationFactTotalHours": 38764.383333333331,
    "hourlyOutput": 89.991486772866665,
    "timeBookingDurationFactTotalMinutes": 2325863,
    "timeFactor": 5.6062047903816348
}
```

```
GET /api2/brand/918e2529-4ab2-4753-9d0e-0613566bb8c4/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "918e2529-4ab2-4753-9d0e-0613566bb8c4",
    "brandName": "Calvin Klein",
    "yearZonedByLocation": 2019,
    "countVisitors": 357670,
    "countReceiptsWithPositiveRevenue": 39728.370010000006,
    "countItemsSoldMinusReturned": 70771.457537,
    "revenueDifference": 2188234.772499,
    "hitRatio": 0.11107548860681636,
    "itemPerReceipt": 1.78138336707965,
    "meanRevenuePerItem": 30.919735846262174
}
```

```
GET /api2/brand/918e2529-4ab2-4753-9d0e-0613566bb8c4/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "918e2529-4ab2-4753-9d0e-0613566bb8c4",
    "brandName": "Calvin Klein",
    "yearZonedByLocation": 2019,
    "countVisitors": 332254,
    "revenuePositiveSum": 2051848.9900000002,
    "revenueNegativeSum": 63896.379999999983,
    "revenueDifferenceSum": 1987952.6099999999,
    "countReceiptsWithPositiveActingItems": 36221,
    "countReceiptsWithPositiveRevenue": 35609,
    "countReceiptsWithNeutralRevenue": 518,
    "countReceiptsWithNegativeRevenue": 520,
    "countItemsSold": 65463,
    "countItemsReturned": 1554,
    "countItemsSoldMinusReturned": 63909,
    "ratioItemsReturnedToAllItems": 0.023188146291239536,
    "timeBookingDurationFactTicks": 966970200000000,
    "hitRatio": 0.10717402950754543,
    "itemPerReceipt": 1.7947429020753181,
    "meanRevenuePerItem": 31.105988358447163,
    "timeBookingDurationFactTotalHours": 26860.283333333333,
    "hourlyOutput": 74.010857790653731,
    "timeBookingDurationFactTotalMinutes": 1611617,
    "timeFactor": 4.8505571039024362
}
```

```
GET /api2/brand/d262bf5a-c248-44dc-8f4a-d7af098b8089/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "d262bf5a-c248-44dc-8f4a-d7af098b8089",
    "brandName": "Tommy Hilfiger",
    "yearZonedByLocation": 2019,
    "countVisitors": 2203570,
    "countReceiptsWithPositiveRevenue": 175427.17587,
    "countItemsSoldMinusReturned": 285512.793539,
    "revenueDifference": 16164221.491604999,
    "hitRatio": 0.079610439364304286,
    "itemPerReceipt": 1.627528871299728,
    "meanRevenuePerItem": 56.614701188151933
}
```

```
GET /api2/brand/d262bf5a-c248-44dc-8f4a-d7af098b8089/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "d262bf5a-c248-44dc-8f4a-d7af098b8089",
    "brandName": "Tommy Hilfiger",
    "yearZonedByLocation": 2019,
    "countVisitors": 2148208,
    "revenuePositiveSum": 15474882.47,
    "revenueNegativeSum": 585226.55999999994,
    "revenueDifferenceSum": 14898432,
    "countReceiptsWithPositiveActingItems": 166069,
    "countReceiptsWithPositiveRevenue": 163270,
    "countReceiptsWithNeutralRevenue": 2246,
    "countReceiptsWithNegativeRevenue": 3596,
    "countItemsSold": 267081,
    "countItemsReturned": 8165,
    "countItemsSoldMinusReturned": 258916,
    "ratioItemsReturnedToAllItems": 0.029664372960914964,
    "timeBookingDurationFactTicks": 4245380400000000,
    "hitRatio": 0.076002882402448929,
    "itemPerReceipt": 1.5858149078214,
    "meanRevenuePerItem": 57.54156560428865,
    "timeBookingDurationFactTotalHours": 117927.23333333332,
    "hourlyOutput": 126.33580538507222,
    "timeBookingDurationFactTotalMinutes": 7075634,
    "timeFactor": 3.2937378503385148
}
```

```
GET /api2/brand/aa552227-c21a-4ab6-b227-7d5ad090e7f3/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "aa552227-c21a-4ab6-b227-7d5ad090e7f3",
    "brandName": "Esprit CZ",
    "yearZonedByLocation": 2019,
    "countVisitors": 1687741,
    "countReceiptsWithPositiveRevenue": 179474.50483,
    "countItemsSoldMinusReturned": 302834.42302600003,
    "revenueDifference": 243558720.38151297,
    "hitRatio": 0.10634007518333677,
    "itemPerReceipt": 1.6873395099367889,
    "meanRevenuePerItem": 804.2636565150392
}
```

```
GET /api2/brand/aa552227-c21a-4ab6-b227-7d5ad090e7f3/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "aa552227-c21a-4ab6-b227-7d5ad090e7f3",
    "brandName": "Esprit CZ",
    "yearZonedByLocation": 2019,
    "countVisitors": 1552321,
    "revenuePositiveSum": 244565530,
    "revenueNegativeSum": 4243478.45,
    "revenueDifferenceSum": 240322051.55,
    "countReceiptsWithPositiveActingItems": 171071,
    "countReceiptsWithPositiveRevenue": 169418,
    "countReceiptsWithNeutralRevenue": 1511,
    "countReceiptsWithNegativeRevenue": 974,
    "countItemsSold": 291662,
    "countItemsReturned": 4052,
    "countItemsSoldMinusReturned": 287610,
    "ratioItemsReturnedToAllItems": 0.013702428697998741,
    "timeBookingDurationFactTicks": null,
    "hitRatio": 0.10913850936758569,
    "itemPerReceipt": 1.6976354342513782,
    "meanRevenuePerItem": 835.58308664510969,
    "timeBookingDurationFactTotalHours": null,
    "hourlyOutput": null,
    "timeBookingDurationFactTotalMinutes": null,
    "timeFactor": null
}
```

```
GET /api2/brand/80385468-ed57-4924-a84a-bac44469b6b8/stats/plan/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "80385468-ed57-4924-a84a-bac44469b6b8",
    "brandName": "More & More CZ",
    "yearZonedByLocation": 2019,
    "countVisitors": 18409,
    "countReceiptsWithPositiveRevenue": 3541.8680600000002,
    "countItemsSoldMinusReturned": 5994.764939,
    "revenueDifference": 7900802.935977,
    "hitRatio": 0.19239872127763596,
    "itemPerReceipt": 1.6925432674078773,
    "meanRevenuePerItem": 1317.9504144652835
}
```

```
GET /api2/brand/80385468-ed57-4924-a84a-bac44469b6b8/stats/fact/yearly/at/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: application/json, text/plain, */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://isp.pth.group/isp2/stats/
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
```

```
{
    "brandId": "80385468-ed57-4924-a84a-bac44469b6b8",
    "brandName": "More & More CZ",
    "yearZonedByLocation": 2019,
    "countVisitors": 18917,
    "revenuePositiveSum": 8804228.5,
    "revenueNegativeSum": 114689.5,
    "revenueDifferenceSum": 8689539,
    "countReceiptsWithPositiveActingItems": 4380,
    "countReceiptsWithPositiveRevenue": 4340,
    "countReceiptsWithNeutralRevenue": 40,
    "countReceiptsWithNegativeRevenue": 8,
    "countItemsSold": 7921,
    "countItemsReturned": 81,
    "countItemsSoldMinusReturned": 7840,
    "ratioItemsReturnedToAllItems": 0.010122469382654336,
    "timeBookingDurationFactTicks": null,
    "hitRatio": 0.22942327007453614,
    "itemPerReceipt": 1.8064516129032258,
    "meanRevenuePerItem": 1108.3595663265305,
    "timeBookingDurationFactTotalHours": null,
    "hourlyOutput": null,
    "timeBookingDurationFactTotalMinutes": null,
    "timeFactor": null
}
```

## Get data from preceding year (2018) for Esprit

```
GET /api2/brand/364bb1c1-1df2-4172-a908-25cc6800d34f/stats/fact/yearly/at/year/2018 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
Upgrade-Insecure-Requests: 1
Cache-Control: max-age=0
```

```
{
    "brandId": "364bb1c1-1df2-4172-a908-25cc6800d34f",
    "brandName": "Esprit",
    "yearZonedByLocation": 2018,
    "countVisitors": 7532338,
    "revenuePositiveSum": 44547289.77,
    "revenueNegativeSum": 1505002.56,
    "revenueDifferenceSum": 46750095.57,
    "countReceiptsWithPositiveActingItems": 822592,
    "countReceiptsWithPositiveRevenue": 880801,
    "countReceiptsWithNeutralRevenue": 5743,
    "countReceiptsWithNegativeRevenue": 23278,
    "countItemsSold": 1457291,
    "countItemsReturned": 40201,
    "countItemsSoldMinusReturned": 1537210,
    "ratioItemsReturnedToAllItems": 0.026845552430330176,
    "timeBookingDurationFactTicks": 14482086600000000,
    "hitRatio": 0.11693593675695382,
    "itemPerReceipt": 1.7452409795175075,
    "meanRevenuePerItem": 30.41230252860702,
    "timeBookingDurationFactTotalHours": 402280.1833333333,
    "hourlyOutput": 116.2127728555359,
    "timeBookingDurationFactTotalMinutes": 24136811,
    "timeFactor": 3.2044248412644256
}
```

## Get two years at once (2018-2019)

```
GET /api2/brand/364bb1c1-1df2-4172-a908-25cc6800d34f/stats/fact/yearly/from/year/2018/to/year/2019 HTTP/1.1
Host: isp.pth.group
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Cookie: experimentation_subject_id=ImI1NDY1MGYxLTJiMmUtNDFhNy04ZDdkLTBmOWFhZjA4OTFjMiI%3D--d37cd9fd5ab33d91290741e2cd54a2106d345cf0
Upgrade-Insecure-Requests: 1
Authorization: NTLM TlRMTVNTUAADAAAAGAAYAHAAAAAQARABiAAAAAAAAABAAAAAGgAaAEAAAAAWABYAWgAAAAAAAAAAAAAABYIIAGEAbgBkAHIAZQAuAHAAcgBlAG4AZABlAGwAVwBPAFIASwBTAFQAQQBUAEkATwBOAAM1fMLZg0i2QnNIuLktbtoYBPY1Y5vmDMMHnFiL3uAdOKGeeC7NBckBAQAAAAAAAACbp+60tdUByV03qvv2qAMAAAAAAgAeAFAAUgBJAFYAQQBUAEUALQBUAEUAWABUAEkATABFAAEADgBQAFQALQBUAEkATABMAAQALABwAHIAaQB2AGEAdABlAC0AdABlAHgAdABpAGwAZQBzAC4AbABvAGMAYQBsAAMAPABwAHQALQB0AGkAbABsAC4AcAByAGkAdgBhAHQAZQAtAHQAZQB4AHQAaQBsAGUAcwAuAGwAbwBjAGEAbAAFACwAcAByAGkAdgBhAHQAZQAtAHQAZQB4AHQAaQBsAGUAcwAuAGwAbwBjAGEAbAAHAAgAxwTw7rS11QEAAAAA
```

```
[
    {
        "brandId": "364bb1c1-1df2-4172-a908-25cc6800d34f",
        "brandName": "Esprit",
        "yearZonedByLocation": 2018,
        "countVisitors": 7532338,
        "revenuePositiveSum": 44547289.77,
        "revenueNegativeSum": 1505002.56,
        "revenueDifferenceSum": 46750095.57,
        "countReceiptsWithPositiveActingItems": 822592,
        "countReceiptsWithPositiveRevenue": 880801,
        "countReceiptsWithNeutralRevenue": 5743,
        "countReceiptsWithNegativeRevenue": 23278,
        "countItemsSold": 1457291,
        "countItemsReturned": 40201,
        "countItemsSoldMinusReturned": 1537210,
        "ratioItemsReturnedToAllItems": 0.026845552430330176,
        "timeBookingDurationFactTicks": 14482086600000000,
        "hitRatio": 0.11693593675695382,
        "itemPerReceipt": 1.7452409795175075,
        "meanRevenuePerItem": 30.41230252860702,
        "timeBookingDurationFactTotalHours": 402280.1833333333,
        "hourlyOutput": 116.2127728555359,
        "timeBookingDurationFactTotalMinutes": 24136811,
        "timeFactor": 3.2044248412644256
    },
    {
        "brandId": "364bb1c1-1df2-4172-a908-25cc6800d34f",
        "brandName": "Esprit",
        "yearZonedByLocation": 2019,
        "countVisitors": 6582899,
        "revenuePositiveSum": 40877138.97,
        "revenueNegativeSum": 1346732.4500000002,
        "revenueDifferenceSum": 42138671.09,
        "countReceiptsWithPositiveActingItems": 707277,
        "countReceiptsWithPositiveRevenue": 741703,
        "countReceiptsWithNeutralRevenue": 5107,
        "countReceiptsWithNegativeRevenue": 19484,
        "countItemsSold": 1280683,
        "countItemsReturned": 34826,
        "countItemsSoldMinusReturned": 1324168,
        "ratioItemsReturnedToAllItems": 0.026473403070598527,
        "timeBookingDurationFactTicks": 13165666800000000,
        "hitRatio": 0.11267118028090663,
        "itemPerReceipt": 1.785307596167199,
        "meanRevenuePerItem": 31.822752921079502,
        "timeBookingDurationFactTotalHours": 365712.9666666667,
        "hourlyOutput": 115.22334434591646,
        "timeBookingDurationFactTotalMinutes": 21942778,
        "timeFactor": 3.3333001159519537
    }
]
```
